<?php

/**
 * @class Ymda_Banner_Widget
 */
class Ymda_Banner_Widget extends WP_Widget {
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'banner_widget',
			__( 'Mostrar Banner' ),
			array( 'description' => __( 'Mostra os banners' ))
		);
	}

	/**
	 * Outputs the content of the widget
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$query = new WP_Query( array(
			'post_type' => 'banner',
			'tax_query' => array(
				array(
					'taxonomy' => 'banner_category',
					'field' => 'slug',
					'terms' => $instance['banner_category']
				)
			),
			'posts_per_page' => isset( $instance['limit'] ) ? $instance['limit']: -1,
			'orderby' => isset( $instance['orderby'] ) ? $instance['orderby']: 'rand'

		) );

		if( $query->have_posts() ) {

			while ( $query->have_posts() ) {
				$query->the_post();

				if ( has_post_thumbnail() ) {
					$link = get_post_meta( get_the_ID(), 'ymda_banner_link', true );
					echo '<a href="' . esc_url( $link ) . '" target="_blank">';
					the_post_thumbnail( 'full', array( 'class' => 'img-responsive sidebar-banner' ) );
					echo '</a>';
				}
			}
		}
	}

	/**
	 * Outputs the options form on admin
	 * @param array $instance The widget options
	 * @return nada
	 */
	public function form( $instance ) {
		$banner_cats = get_terms( 'banner_category' );
?>
		<p>
            <label for="<?php echo $this->get_field_id( 'banner_category' ); ?>"><?php _e( 'Selecione a categoria do banner:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'banner_category' ); ?>" name="<?php echo $this->get_field_name( 'banner_category' ); ?>">
				<?php
				foreach ( $banner_cats as $banner_cat ) {
					echo '<option value="' .  $banner_cat->slug  . '"'
					     . selected( $instance['banner_category'], $banner_cat->slug, false )
					     . '>' . $banner_cat->name . "</option>\n";
				}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><?php _e( 'Selecione a ordem em que os banners apareceram:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
				<?php
					$banner_orders = array(
						'rand' => 'Aleat&#243rio',
						'ID' => 'Por ID',
						'author' => 'Por Autor',
						'title' => 'Por T&#237;tulo',
						'name' => 'Por Nome',
						'type' => 'Por tipo',
						'date' => 'Por data',
						'modified' => 'Por &#250;ltima data de modifica&#231;&#227o'
					);

					foreach  ( $banner_orders as $banner_order => $banner_desc ) {
						echo '<option value="' . $banner_order . '"'
							. selected( $instance['orderby'], $banner_order, true )
							. '>' . $banner_desc . "</option>\n";
					}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e( 'Selecione o limite de posts:' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" value="<?php echo $instance['limit']; ?>">
		</p>
	<?php
	}

	/**
	 * Processing widget options on save
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 * @return array $instance
	 */
	public function update( $new_instance, $old_instance ) {
		$new_instance = (array) $new_instance;

		$instance['banner_category'] = $new_instance['banner_category'];
		$instance['limit'] = $new_instance['limit'];
		$instance['orderby'] = $new_instance['orderby'];

		return $instance;
	}
}
